all: render

SITEDIR=./site

serve:
	@./scripts/serve.sh $(SITEDIR)

render:
	rsync -curva ./src/ $(SITEDIR)/
	bash ./scripts/render-all.sh

watch:
	(find ./src -type f ; find ./parts -type f) | entr -c make render 

clean:
	rm -rf $(SITEDIR)
	mkdir $(SITEDIR)
