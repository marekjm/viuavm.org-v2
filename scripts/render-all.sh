#!/usr/bin/env bash

set -e

SITE=./site

find ${SITE} -type f -name '*.html' |
    xargs -n 1 --verbose --no-run-if-empty ./scripts/render-one.sh

# grep -Prl '<!-- *blog::render *= *true *-->' ${SITE} |
#     xargs -n 1 --verbose --no-run-if-empty ./scripts/render-one.sh
