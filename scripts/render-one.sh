#!/usr/bin/env bash

set -e

DOCUMENT=${1}

# TITLE=$(sed 's/<!-- *blog::title *= *\(.*\) -->/\1/;t;d' ${DOCUMENT})
# if [[ ${TITLE} == '' ]]; then
#     1>&2 echo "  warning: using default title"
# else
#     1>&2 echo "  title = [${TITLE}]"
# fi

ID=$(sha384sum ${DOCUMENT} | awk '{ print $1 }')
TEMP=/tmp/${ID}

cat ${DOCUMENT} > ${TEMP}

# SEP=$'\001'

# cat parts/head.html > ${TEMP}
# if [[ ${TITLE} != '' ]]; then
#     sed -i s${SEP}'<title>.*</title>'${SEP}"<title>${TITLE} -- Ozro</title>"${SEP} ${TEMP}
# fi

# cat ${DOCUMENT} |
#     sed 's/^<!-- blog:://;T;d' >> ${TEMP}

# cat parts/tail.html >> ${TEMP}

m4 ${TEMP} > ${DOCUMENT}
rm ${TEMP}
