#!/usr/bin/env bash

set -e

PORT=30394

echo "serving $(pwd)/site at [::]:${PORT}"

webfsd -s -l - -F -6 -p ${PORT} -r $(pwd)/${1}
